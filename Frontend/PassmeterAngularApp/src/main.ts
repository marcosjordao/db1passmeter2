import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

export function getBaseApiUrl() {
  return 'https://localhost:44317/api/v1';
  // return 'https://passmeterapi.azurewebsites.net/api/v1';
}

const providers = [
  { provide: 'BASE_API_URL', useFactory: getBaseApiUrl, deps: [] }
];

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic(providers).bootstrapModule(AppModule)
  .catch(err => console.error(err));

