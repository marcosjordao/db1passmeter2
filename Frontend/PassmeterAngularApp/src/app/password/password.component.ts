import { Component, OnInit, Input } from '@angular/core';
import { PassmeterService } from '../services/passmeter.service';
import { PassmeterResult } from '../model/passmeterResult';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  @Input() password: string;
  result: PassmeterResult;
  complexityClass: string;

  constructor(private passmeterService: PassmeterService) { }

  ngOnInit() {
    this.result = new PassmeterResult(0, 'TOO_SHORT', 'Muito curta');
    this.complexityClass = this.getComplexityClass(this.result.complexity);
  }

  checkPassword(password: string) {
    this.passmeterService.calculate(password)
      .subscribe(result => {
        this.result = result;
        this.complexityClass = this.getComplexityClass(this.result.complexity);
      });
  }

  getComplexityClass(complexity: string): string {
    switch (complexity) {
      case 'TOO_SHORT':
      case 'VERY_WEAK':
        return 'danger';

      case 'WEAK':
        return 'warning';

      case 'GOOD':
        return 'info';

      case 'STRONG':
      case 'VERY_STRONG':
        return 'success';
    }
  }
}
