export class PassmeterResult {
    score: number;
    complexity: string;
    complexityDescription: string;

    constructor(score: number, complexity: string, complexityDescription: string) {
        this.score = score;
        this.complexity = complexity;
        this.complexityDescription = complexityDescription;
    }
}
