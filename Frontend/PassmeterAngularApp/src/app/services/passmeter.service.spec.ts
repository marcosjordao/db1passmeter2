import { TestBed } from '@angular/core/testing';

import { PassmeterService } from './passmeter.service';

describe('PassmeterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PassmeterService = TestBed.get(PassmeterService);
    expect(service).toBeTruthy();
  });
});
