import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PassmeterResult } from '../model/passmeterResult';
import { PassmeterRequest } from '../model/passmeterRequest';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PassmeterService {

  private apiUrl = `${this.baseUrl}/passmeter`;

  constructor(private http: HttpClient,
    @Inject('BASE_API_URL') private baseUrl: string) { }

  calculate(password: string) {
    const passmeterRequest = new PassmeterRequest(password);
    return this.http.post<PassmeterResult>(this.apiUrl, passmeterRequest, httpOptions);
  }
}
