﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Passmeter.Domain.Calculator
{
    public interface IPassmeterCalculator
    {
     
        int CalculateScore(string password);

    }
}
