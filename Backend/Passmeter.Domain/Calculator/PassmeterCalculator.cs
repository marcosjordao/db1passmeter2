﻿using System;
using System.Collections.Generic;
using System.Text;
using Passmeter.Domain.Enums;
using Passmeter.Domain.Extension;

namespace Passmeter.Domain.Calculator
{
    public class PassmeterCalculator : IPassmeterCalculator
    {

        private const string ALPHAS = "abcdefghijklmnopqrstuvwxyz";
        private const string NUMERICS = "01234567890";
        private const string SYMBOLS = ")!@#$%^&*()";

        private const int MIN_LENGTH = 8;

        private const int MULT_LENGTH = 4;
        private const int MULT_NUMBER = 4;
        private const int MULT_SYMBOL = 6;
        private const int MULT_MID_CHAR = 2;
        private const int MULT_CONSEC_ALPHA = 2;
        private const int MULT_CONSEC_NUMBER = 2;
        private const int MULT_SEQUENTIAL_ALPHA = 3;
        private const int MULT_SEQUENTIAL_NUMBER = 3;
        private const int MULT_SEQUENTIAL_SYMBOL = 3;

        public int CalculateScore(string password)
        {
            int score = 0;

            if (!String.IsNullOrWhiteSpace(password))
            {
                int uppercase = 0;
                int lowercase = 0;
                int consecutiveUppercase = 0;
                int consecutiveLowercase = 0;
                int numbers = 0;
                int consecutiveNumbers = 0;
                int symbols = 0;
                int middleNumbersOrSymbols = 0;
                double repeatInc = 0;
                int repeatedChar = 0;
                int uniqueChar = 0;
                int sequentialChar = 0;
                int sequentialAlpha = 0;
                int sequentialNumber = 0;
                int sequentialSymbol = 0;

                score = (password.Length * MULT_LENGTH);


                /* Loop through password to check for Symbol, Numeric, Lowercase and Uppercase pattern matches */
                char prevChar = Char.MinValue;
                for (var currentIndex = 0; currentIndex < password.Length; currentIndex++)
                {
                    var currentChar = password[currentIndex];

                    if (Char.IsLetter(currentChar))
                    {
                        if (Char.IsUpper(currentChar))
                        {
                            uppercase++;
                            if (Char.IsUpper(prevChar))
                                consecutiveUppercase++;
                        }
                        else if (Char.IsLower(currentChar))
                        {
                            lowercase++;
                            if (Char.IsLower(prevChar))
                                consecutiveLowercase++;
                        }
                    }
                    else if (Char.IsDigit(currentChar))
                    {
                        numbers++;
                        if (currentIndex > 0 && currentIndex != password.Length - 1)
                            middleNumbersOrSymbols++;
                        if (Char.IsDigit(prevChar))
                            consecutiveNumbers++;
                    }
                    else if (Char.IsSymbol(currentChar))
                    {
                        symbols++;
                        if (currentIndex > 0 && currentIndex != password.Length - 1)
                        {
                            middleNumbersOrSymbols++;
                        }
                    }
                    prevChar = currentChar;

                    /* Internal loop through password to check for repeat characters */
                    var bCharExists = false;
                    for (var repeatIndex = 0; repeatIndex < password.Length; repeatIndex++)
                    {
                        if (currentChar == password[repeatIndex] && currentIndex != repeatIndex)
                        { /* repeat character exists */
                            bCharExists = true;
                            /* 
                            Calculate icrement deduction based on proximity to identical characters
                            Deduction is incremented each time a new match is discovered
                            Deduction amount is based on total password length divided by the
                            difference of distance between currently selected match
                            */
                            repeatInc += Math.Abs(password.Length / (repeatIndex - currentIndex));
                        }
                    }
                    if (bCharExists)
                    {
                        repeatedChar++;
                        uniqueChar = password.Length - repeatedChar;
                        repeatInc = (uniqueChar > 0) ? Math.Ceiling(repeatInc / uniqueChar) : Math.Ceiling(repeatInc);
                    }
                }

                /* Check for sequential alpha string patterns (forward and reverse) */
                for (var s = 0; s < ALPHAS.Length - 3; s++)
                {
                    var sFwd = ALPHAS.Substring(s, 3);
                    var sRev = sFwd.Reverse();
                    if (password.ToLower().Contains(sFwd) || password.ToLower().Contains(sRev))
                    {
                        sequentialAlpha++;
                        sequentialChar++;
                    }
                }

                /* Check for sequential numeric string patterns (forward and reverse) */
                for (var s = 0; s < NUMERICS.Length - 3; s++)
                {
                    var sFwd = NUMERICS.Substring(s, 3);
                    var sRev = sFwd.Reverse();
                    if (password.ToLower().Contains(sFwd) || password.ToLower().Contains(sRev))
                    {
                        sequentialNumber++;
                        sequentialChar++;
                    }
                }

                /* Check for sequential symbol string patterns (forward and reverse) */
                for (var s = 0; s < SYMBOLS.Length - 3; s++)
                {
                    var sFwd = SYMBOLS.Substring(s, 3);
                    var sRev = sFwd.Reverse();
                    if (password.ToLower().Contains(sFwd) || password.ToLower().Contains(sRev))
                    {
                        sequentialSymbol++;
                        sequentialChar++;
                    }
                }

                /* General point assignment */
                if (uppercase > 0 && uppercase < password.Length)
                {
                    score = score + ((password.Length - uppercase) * 2);
                }
                if (lowercase > 0 && lowercase < password.Length)
                {
                    score = score + ((password.Length - lowercase) * 2);
                }
                if (numbers > 0 && numbers < password.Length)
                {
                    score = score + (numbers * MULT_NUMBER);
                }
                if (symbols > 0)
                {
                    score = score + (symbols * MULT_SYMBOL);
                }
                if (middleNumbersOrSymbols > 0)
                {
                    score = score + (middleNumbersOrSymbols * MULT_MID_CHAR);
                }

                /* Point deductions for poor practices */
                if ((lowercase > 0 || uppercase > 0) && symbols == 0 && numbers == 0)
                {  // Only Letters
                    score = score - password.Length;
                    var nAlphasOnly = password.Length;
                }
                if (lowercase == 0 && uppercase == 0 && symbols == 0 && numbers > 0)
                {  // Only Numbers
                    score = score - password.Length;
                    var nNumbersOnly = password.Length;
                }
                if (repeatedChar > 0)
                {  // Same character exists more than once
                    score = score - Convert.ToInt32(repeatInc);
                }
                if (consecutiveUppercase > 0)
                {  // Consecutive Uppercase Letters exist
                    score = score - (consecutiveUppercase * MULT_CONSEC_ALPHA);
                }
                if (consecutiveLowercase > 0)
                {  // Consecutive Lowercase Letters exist
                    score = score - (consecutiveLowercase * MULT_CONSEC_ALPHA);
                }
                if (consecutiveNumbers > 0)
                {  // Consecutive Numbers exist
                    score = score - (consecutiveNumbers * MULT_CONSEC_NUMBER);
                }
                if (sequentialAlpha > 0)
                {  // Sequential alpha strings exist (3 characters or more)
                    score = score - (sequentialAlpha * MULT_SEQUENTIAL_ALPHA);
                }
                if (sequentialNumber > 0)
                {  // Sequential numeric strings exist (3 characters or more)
                    score = score - (sequentialNumber * MULT_SEQUENTIAL_NUMBER);
                }
                if (sequentialSymbol > 0)
                {  // Sequential symbol strings exist (3 characters or more)
                    score = score - (sequentialSymbol * MULT_SEQUENTIAL_SYMBOL);
                }

                /* Determine if mandatory requirements have been met */
                int[] arrChars = { password.Length, uppercase, lowercase, numbers, symbols };
                string[] arrCharsIds = { "nLength", "nAlphaUC", "nAlphaLC", "nNumber", "nSymbol" };
                var nReqChar = 0;
                var nRequirements = 0;
                var nMinReqChars = 0;

                for (var c = 0; c < arrChars.Length; c++)
                {
                    var minVal = 0;
                    if (arrCharsIds[c] == "nLength")
                    {
                        minVal = MIN_LENGTH - 1;
                    }

                    if (arrChars[c] == minVal + 1)
                        nReqChar++;
                    else if (arrChars[c] > minVal + 1)
                        nReqChar++;
                }
                nRequirements = nReqChar;
                if (password.Length >= MIN_LENGTH)
                    nMinReqChars = 3;
                else
                    nMinReqChars = 4;
                if (nRequirements > nMinReqChars)
                {
                    score = score + (nRequirements * 2);
                }

                // Min 0 Max 100
                if (score > 100)
                    score = 100;
                else if (score < 0)
                    score = 0;
            }

            return score;
        }

        public EnumComplexity GetComplexityByScore(int score)
        {

            if (score > 0 && score < 20)
                return EnumComplexity.VERY_WEAK;

            else if (score >= 20 && score < 40)
                return EnumComplexity.WEAK;

            else if (score >= 40 && score < 60)
                return EnumComplexity.GOOD;

            else if (score >= 60 && score < 80)
                return EnumComplexity.STRONG;

            else if (score >= 80)
                return EnumComplexity.VERY_STRONG;

            return EnumComplexity.TOO_SHORT;
        }

    }
}
