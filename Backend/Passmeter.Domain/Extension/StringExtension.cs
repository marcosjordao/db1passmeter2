﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Passmeter.Domain.Extension
{
    public static class StringExtension
    {
        public static string Reverse(this string str)
        {
            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);

            return new string(charArray);
        }
    }
}