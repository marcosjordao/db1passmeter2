﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Passmeter.Domain.Enums
{
    public enum EnumComplexity
    {
        [Description("Muito curta")]
        TOO_SHORT,

        [Description("Muito fraca")]
        VERY_WEAK,

        [Description("Fraca")]
        WEAK,

        [Description("Boa")]
        GOOD,

        [Description("Forte")]
        STRONG,

        [Description("Muito forte")]
        VERY_STRONG

    }
}
