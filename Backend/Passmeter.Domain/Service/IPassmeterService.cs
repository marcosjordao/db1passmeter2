﻿using Passmeter.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Passmeter.Domain.Service
{
    public interface IPassmeterService
    {

        PassmeterResponse Calculate(string password);

    }
}
