﻿using System;
using System.Collections.Generic;
using System.Text;
using Passmeter.Domain.Calculator;
using Passmeter.Domain.DTO;
using Passmeter.Domain.Extension;

namespace Passmeter.Domain.Service
{
    public class PassmeterService: IPassmeterService
    {
        public PassmeterResponse Calculate(string password)
        {
            var Calculator = new PassmeterCalculator();
            var scoreCalculated = Calculator.CalculateScore(password);
            var complexity = Calculator.GetComplexityByScore(scoreCalculated);

            return new PassmeterResponse(scoreCalculated, 
                                         complexity.ToString(),
                                         complexity.GetDescription());
        }
    }
}
