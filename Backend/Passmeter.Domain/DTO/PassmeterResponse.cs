﻿using Passmeter.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Passmeter.Domain.DTO
{
    public class PassmeterResponse
    {
        public int Score { get; }
        public string Complexity { get; }
        public string ComplexityDescription { get; }

        public PassmeterResponse(int score, string complexity, string complexityDescription)
        {
            this.Score = score;
            this.Complexity = complexity;
            this.ComplexityDescription = complexityDescription;
        }
    }
}
