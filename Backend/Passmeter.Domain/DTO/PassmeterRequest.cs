﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Passmeter.Domain.DTO
{
    public class PassmeterRequest
    {
        public string Password { get; set; }

        public PassmeterRequest(string password)
        {
            Password = password;
        }
    }
}
