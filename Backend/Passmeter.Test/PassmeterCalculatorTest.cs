﻿using Passmeter.Domain.Calculator;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Passmeter.Test
{
    public class PassmeterCalculatorTest
    {
        [Theory]
        [InlineData("", 0)]
        [InlineData("mf", 4)]
        [InlineData("ABCdef", 10)]
        [InlineData("Fracas", 21)]
        [InlineData("SenhaBoa0", 59)]
        [InlineData("SenhaForte0", 67)]
        [InlineData("SenhaMuito0Fort3", 100)]
        public void TestScoreCalculated(string password, int score)
        {
            var passmeterCalculator = new PassmeterCalculator();

            var scoreCalculated = passmeterCalculator.CalculateScore(password);

            Assert.Equal(score, scoreCalculated);
        }
    }
}
