﻿using Passmeter.Domain.Service;
using Xunit;


namespace Passmeter.Test
{
    public class PassmeterServiceTest
    {
        [Theory]
        [InlineData("", 0, "Muito curta")]
        [InlineData("mf", 4, "Muito fraca")]
        [InlineData("ABCdef", 10, "Muito fraca")]
        [InlineData("Fracas", 21, "Fraca")]
        [InlineData("SenhaBoa0", 59, "Boa")]
        [InlineData("SenhaForte0", 67, "Forte")]
        [InlineData("SenhaMuito0Fort3", 100, "Muito forte")]
        public void TestScoreCalculated(string password, int score, string complexity)
        {
            var passmeterService = new PassmeterService();

            var result = passmeterService.Calculate(password);

            Assert.Equal(score, result.Score);
            Assert.Equal(complexity, result.Complexity);
        }
    }
}
