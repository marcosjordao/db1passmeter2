﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Passmeter.Domain.DTO;
using Passmeter.Domain.Service;

namespace Passmeter.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PassmeterController : ControllerBase
    {

        private readonly IPassmeterService _passmeterService;

        public PassmeterController(IPassmeterService passmeterService)
        {
            _passmeterService = passmeterService;
        }

        // POST api/passmeter
        [HttpPost]
        public PassmeterResponse Post([FromBody] PassmeterRequest requestParam)
        {
            return _passmeterService.Calculate(requestParam.Password);
        }

    }
}