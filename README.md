# PassMeter

Software de avaliação de segurança de uma senha.

Regras baseadas no site http://www.passwordmeter.com.

Solução apresentada como "Prova Técnica - Desenvolvedor .NET" para a empresa DB1 Global Software.


## Tecnologias utilizadas

 - **Backend**: Rest API - [.NET Core 2.1](https://www.microsoft.com/net)
 - **Frontend**: [Angular 6](https://angular.io/)
 - **Outros**: [Bootstrap 4](http://getbootstrap.com/), [xUnit](https://xunit.github.io/), [Docker](https://www.docker.com/)

## Como executar
### 1. Online

Foi efetuado o deploy da aplicação no Azure.

Pode ser acessada em:
> http://passmeter.azurewebsites.net

A API pode ser acessada em:
> https://passmeterapi.azurewebsites.net/swagger

### 2. Execução local
#### 2.1. Backend
Na raiz do código fonte execute:
```
cd Backend/Passmeter.Api
dotnet run
```
#### 2.2. Frontend
Na raiz do código fonte execute:
```
cd Frontend/PassmeterAngularApp
ng serve --open
```
### 3. Docker
A aplicação possui imagens para execução via container Docker.

Na raiz do código fonte execute:
```
docker-compose build
docker-compose up
```
Ficará disponível em: https://localhost:4200/